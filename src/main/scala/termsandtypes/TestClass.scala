package scala.termsandtypes

class Point(val xc: Int, val yc: Int) {
  var x: Int = xc
  var y: Int = yc

  def move(dx: Int, dy: Int) {
    x = x + dx
    y = y + dy
    println ("Point x 1location : " + x);
    println ("Point y 1location : " + y);
  }
}

class Location(override val xc: Int, override val yc: Int,
               val zc :Int) extends Point(xc, yc){
  var z: Int = zc

  def move(dx: Int, dy: Int, dz: Int) {
    x = x + dx
    y = y + dy
    z = z + dz
    println ("Point x 2 location : " + x);
    println ("Point y 2 location : " + y);
    println ("Point z 2 location : " + z);
  }
}

object TestClass {
  def main(args: Array[String]) {
    val pt = new Point(10, 20);

    // Move to a new location
    pt.move(10, 10);
    printPoint

    def printPoint{
      println ("Point x 3 location : " + pt.x);
      println ("Point y 3 location : " + pt.y);
    }
  }
}