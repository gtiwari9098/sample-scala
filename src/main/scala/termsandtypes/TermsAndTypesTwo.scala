package scala.termsandtypes

object TermsAndTypesTwo {

  def upperCaseMethod(): String = {
    "Hello, Scala!".toUpperCase//Invoke a method here making all of the letters uppercase
  }

  def absMethod(): Int = {
    -42.abs//invoke a method here to return an absolute value of the number
  }
//What is the range of numbers between 1 and 10?

  def getRangeOfNumbers(): Range.Inclusive = {
    1.to(10);
  }
  def main(args: Array[String]): Unit = {
    println(upperCaseMethod())
    println(absMethod())
    println(getRangeOfNumbers().start)
  }

}