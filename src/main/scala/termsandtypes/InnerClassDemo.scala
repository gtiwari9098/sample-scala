package scala.termsandtypes

class InnerClassDemo {

  private[termsandtypes] class Inner {
    private def f = println("f")

    private[termsandtypes] class InnerMost {
     // def this // OK

    }

  }

  // (new Inner).f() // Error: f is not accessible
}