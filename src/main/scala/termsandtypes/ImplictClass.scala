package scala.termsandtypes

import Run._

object ImplictClassDemo {
  def main(args: Array[String]) {
    4 times println("hello")
  }
}
object Run {
  implicit class IntTimes(x: Int) {
    def times [A](f: =>A): Unit = {
      def loop(current: Int): Unit =

        if(current > 0){
          f
          loop(current - 1)
        }
      loop(x)
    }
  }
}