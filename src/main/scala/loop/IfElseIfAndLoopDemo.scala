package scala.loop

object IfElseIfAndLoopDemo {
  def main(args: Array[String]) {
    var x = 30;

    if( x == 10 ){
      println("Value of X is 10");
    } else if( x == 20 ){
      println("Value of X is 20");
    } else if( x == 30 ){
      println("Value of X is 30");
    } else{
      println("This is else statement");
    }

    var y:Int =10;
    if (x == 30) {
      if (y == 10) {
        println("X = 30 and Y = 10")
      }
    }

    // While Loop
    var a = 10;

    // An infinite loop.
    while( true ){
      println( "Value of a: " + a );
    }

    // For loop
    var v: Int = 0
    var n: Int = 0
    for( v <- 1 to 10){
      println( "Value of a: " + v );
    }
    // for loop execution with a range
    for( n <- 1 until 10){
      println( "Value of a: " + n );
    }

    // for loop execution with a range
    for( a <- 1 to 3; b <- 1 to 3){
      println( "Value of a: " + a );
      println( "Value of b: " + b );
    }


    // For loop In collection
    var as = 0;
    val numList = List(1,2,3,4,5,6);

    // for loop execution with a collection
    for( as <- numList ){
      println( "Value of a: " + a );
    }

    // For loop with filters
    var ae = 0;
    val numListOne = List(1,2,3,4,5,6,7,8,9,10);

    // for loop execution with multiple filters
    for( ae <- numListOne
         if ae != 3; if ae < 8 ){
      println( "Value of a: " + ae );
    }

    // for loop execution with a yield
    //You can store return values from a "for" loop in a variable or can return through a function
    var retVal = for{ ae <- numList if ae != 3; if ae < 8 }yield ae

    // Now print returned values using another loop.
    for( ae <- retVal){
      println( "Value of a: " + ae );
    }

    // Do While Loop
    do {
      println( "Value of a: " + a );
      a = a + 1;
    }
    while( a < 20 )
  }
}
