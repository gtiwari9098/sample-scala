package scala.collection

//Set is a collection that contains no duplicate elements.
//There are two kinds of Sets, the immutable and the mutable.
// The difference between mutable and immutable objects is that when an object is immutable,
// the object itself can't be changed.
object SetsDemo {

  def main(args: Array[String]): Unit = {
        var s = Set(1,3,5,7)

    // Basic example
    val fruit :Set[String] = Set("apples","Orange" ,"pear")
    val nums :Set[Int] = Set() // empty set

    println(" head of the fruit "+fruit.head);
    println(" Tail of the fruit "+fruit.tail);
    println( "Check if fruit is empty : " + fruit.isEmpty )
    println( "Check if nums is empty : " + nums.isEmpty )


    //
    val fruit1 = Set("apples", "oranges", "pears")
    val fruit2 = Set("mangoes", "banana")

    // use two or more sets with ++ as operator
    var fruitjoined = fruit1 ++ fruit2
    println( "fruit1 ++ fruit2 : " + fruitjoined)

    // use two sets with ++ as method
    fruitjoined = fruit1.++(fruit2)
    println( "fruit1.++(fruit2) : " + fruitjoined)


    // Find common values from two set
    val num1 = Set(5,6,9,20,30,45)
    val num2 = Set(50,60,9,20,35,55)

    // find common elements between two sets
    println( "num1.&(num2) : " + num1.&(num2) )
    println( "num1.intersect(num2) : " + num1.intersect(num2) )


  }
}
