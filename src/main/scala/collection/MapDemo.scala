package scala.collection

//Scala map is a collection of key/value pairs
/*
Maps are also called Hash tables
There are two kinds of Maps, the immutable and the mutable.
The difference between mutable and immutable objects is that when an object is immutable, the object itself can't be changed.

By default, Scala uses the immutable Map

If you want to use the mutable Map,
you'll have to import scala.collection.mutable.Map class explicitly.

If you want to use both mutable and immutable Maps in the same, then you
can continue to refer to the immutable Map as Map but you
can refer to the mutable set as mutable.Map.
 */
object MapDemo {

  def main(args: Array[String]): Unit = {

    // Empty hash table whose keys are strings and values are integers:
    var nums:Map[Char,Int] = Map()

    // A map with keys and values.
       val colors = Map("red" -> "#FF0000", "azure" -> "#F0FFFF", "peru" -> "#CD853F")

    println( "Keys in colors : " + colors.keys )
    println( "Values in colors : " + colors.values )
    println( "Check if colors is empty : " + colors.isEmpty )
    println( "Check if nums is empty : " + nums.isEmpty )

    colors.keys.foreach{ i =>
      print( "Key = " + i )
      println(" Value = " + colors(i) )}
    // Concateneting Map

    val colorsOne = Map("red" -> "#FF0000", "azure" -> "#F0FFFF", "peru" -> "#CD853F")
    val colorsTwo = Map("blue" -> "#0033FF", "yellow" -> "#FFFF00", "red" -> "#FF0000")

    // use two or more Maps with ++ as operator
    var colorsJoined = colorsOne ++ colorsTwo
    println( "colors1 ++ colors2 : " + colorsJoined )

    // use two maps with ++ as method
    colorsJoined = colorsOne.++(colorsTwo)
    println( "colors1.++(colors2)) : " + colorsJoined )
  }
}
