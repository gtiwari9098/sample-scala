package scala.collection

/*
Scala Lists are quite similar to arrays which means,
all the elements of a list have the same type but there are two important differences.
 First, lists are immutable, which means elements of a list cannot be changed by assignment.
 Second, lists represent a linked list whereas arrays are flat.
 */
object ListDemo {
  def main(args: Array[String]) {

    // List of Strings
    val fruit: List[String] = List("apples", "oranges", "pears")

    // List of Integers
    val nums: List[Int] = List(1, 2, 3, 4)

    // Empty List.
    val empty: List[Nothing] = List()

    // Two dimensional list
    val dim: List[List[Int]] =
      List(
        List(1, 0, 0),
        List(0, 1, 0),
        List(0, 0, 1)
      )

   // All lists can be defined using two fundamental building blocks,
    // a tail Nil and ::, which is pronounced cons.
    //Nil also represents the empty list. All the above lists can be defined as follows.

    // List of Strings
    val fruitList = "apples" :: ("oranges" :: ("pears" :: Nil))
    // List of Integers
    val numslist = 1 :: (2 :: (3 :: (4 :: Nil)))
    // Empty List.
    val emptyone = Nil
    // Two dimensional list
    val dimtest = (1 :: (0 :: (0 :: Nil))) ::
      (0 :: (1 :: (0 :: Nil))) ::
      (0 :: (0 :: (1 :: Nil))) :: Nil

    println( "Before reverse fruit : " + fruitList )
    println( "After reverse fruit : " + fruitList.reverse )

    //Concatenating list

    val fruit1 = "apples" :: ("oranges" :: ("pears" :: Nil))
    val fruit2 = "mangoes" :: ("banana" :: Nil)

    // use two or more lists with ::: operator
    var fruitjoined = fruit1 ::: fruit2
    println( "fruit1 ::: fruit2 : " + fruitjoined )

    // use two lists with Set.:::() method
    fruitjoined = fruit1.:::(fruit2)
    println( "fruit1.:::(fruit2) : " + fruitjoined )

    // pass two or more lists as arguments
    fruitjoined = List.concat(fruit1, fruit2)
    println( "List.concat(fruit1, fruit2) : " + fruitjoined  )

    //You can use List.fill() method creates a list consisting of zero or more copies of the same element.
    // Try the following example program.


    val fruitfill = List.fill(3)("apples") // Repeats apples three times.
    println( "fruit : " + fruitfill  )

    val num = List.fill(10)(2)         // Repeats 2, 10 times.
    println( "num : " + num  )


    //You can use a function along with List.tabulate() method to apply on all the elements of the list
    // before tabulating the list.
    // Its arguments are just like those of List.fill:
    // the first argument list gives the dimensions of the list to create,
    // and the second describes the elements of the list.
    // The only difference is that instead of the elements being fixed,
    // they are computed from a function.

    // Creates 5 elements using the given function.
    val squares = List.tabulate(6)(n => n * n)
    println( "squares : " + squares  )

    val mul = List.tabulate( 4,5 )( _ * _ )
    println( "mul : " + mul  )
  }
}
